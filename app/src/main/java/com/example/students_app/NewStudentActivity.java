package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.students_app.model.Model;
import com.example.students_app.model.Student;

public class NewStudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_student);

        EditText name = findViewById(R.id.new_student_name);
        EditText id = findViewById(R.id.new_student_id);
        EditText phone = findViewById(R.id.new_student_phone);
        EditText address = findViewById(R.id.new_student_address);
        CheckBox checkStatus = findViewById(R.id.new_student_checkStatus);
        Button saveButton = findViewById(R.id.new_student_save);
        Button cancelButton = findViewById(R.id.new_student_cancel);

        saveButton.setOnClickListener(view -> {
            Student newStudent = new Student(name.getText().toString(),
                                             id.getText().toString(),
                                             phone.getText().toString(),
                                             address.getText().toString(),
                                             checkStatus.isChecked());
            Model.instance().addStudent(newStudent);
            finish();
            startActivity(new Intent(NewStudentActivity.this, MainActivity.class));
        });

        cancelButton.setOnClickListener(view -> finish());
    }
}