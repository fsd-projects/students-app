package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.students_app.model.Model;
import com.example.students_app.model.Student;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Student> students;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        students = Model.instance().getAllStudents();
        RecyclerView studentsList = findViewById(R.id.students_list);
        studentsList.setHasFixedSize(true);
        StudentRecyclerAdapter adapter = new StudentRecyclerAdapter(getLayoutInflater(), students);
        studentsList.setAdapter(adapter);
        studentsList.setLayoutManager(new LinearLayoutManager(this));

        adapter.setOnItemClickListener(new StudentRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Student studentCLicked = students.get(pos);
                startActivity(new Intent(MainActivity.this, StudentDetailsActivity.class).putExtra("student", studentCLicked));
            }
        });

        FloatingActionButton addStudent = findViewById(R.id.students_list_add);
        addStudent.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, NewStudentActivity.class));
        });
    }
}