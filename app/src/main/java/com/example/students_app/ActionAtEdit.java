package com.example.students_app;

import java.io.Serializable;

public enum ActionAtEdit implements Serializable {
    CANCEL,
    DELETE,
    SAVE
}
