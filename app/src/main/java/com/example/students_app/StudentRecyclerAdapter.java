package com.example.students_app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.students_app.model.Student;

import java.util.List;

class StudentViewHolder extends RecyclerView.ViewHolder{
    TextView name;
    TextView id;
    CheckBox checkStatus;
    List<Student> data;

    public StudentViewHolder(@NonNull View itemView, StudentRecyclerAdapter.OnItemClickListener listener, List<Student> data) {
        super(itemView);
        this.data = data;
        name = itemView.findViewById(R.id.student_list_row_name);
        id = itemView.findViewById(R.id.student_list_row_id);
        checkStatus = itemView.findViewById(R.id.checkStatus);
        checkStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int)checkStatus.getTag();
                Student student = data.get(pos);
                student.checkStatus = checkStatus.isChecked();
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                listener.onItemClick(pos);
            }
        });
    }

    public void bind(Student student, int index) {
        name.setText(student.name);
        id.setText(student.id);
        checkStatus.setChecked(student.checkStatus);
        checkStatus.setTag(index);
    }
}

public class StudentRecyclerAdapter extends RecyclerView.Adapter<StudentViewHolder> {

    OnItemClickListener listener;
    public static interface OnItemClickListener{
        void onItemClick(int pos);
    }

    LayoutInflater inflater;
    List<Student> data;
    public StudentRecyclerAdapter(LayoutInflater inflater, List<Student> data){
        this.inflater = inflater;
        this.data = data;
    }

    void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.student_list_row,parent,false);
        return new StudentViewHolder(view,listener, data);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int index) {
        Student st = data.get(index);
        holder.bind(st,index);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}
