package com.example.students_app;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.students_app.model.Student;

public class StudentDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);
        Student student = (Student) getIntent().getSerializableExtra("student");
        TextView name = findViewById(R.id.student_details_name_value);
        name.setText(student.name);
        TextView id = findViewById(R.id.student_details_id_value);
        id.setText(student.id);
        TextView phone = findViewById(R.id.student_details_phone_value);
        phone.setText(student.phone);
        TextView address = findViewById(R.id.student_details_address_value);
        address.setText(student.address);
        CheckBox checkStatus = findViewById(R.id.student_details_check_status);
        checkStatus.setChecked(student.checkStatus);
        Button editButton = findViewById(R.id.student_details_edit);
        editButton.setOnClickListener(view -> {
            startActivity(new Intent(StudentDetailsActivity.this, EditStudentActivity.class).putExtra("student", student));
        });
    }
}