package com.example.students_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.students_app.model.Model;
import com.example.students_app.model.Student;

public class EditStudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);
        Student student = (Student) getIntent().getSerializableExtra("student");
        EditText name = findViewById(R.id.student_edit_name_value);
        name.setText(student.name);
        EditText id = findViewById(R.id.student_edit_id_value);
        id.setText(student.id);
        EditText phone = findViewById(R.id.student_edit_phone_value);
        phone.setText(student.phone);
        EditText address = findViewById(R.id.student_edit_address_value);
        address.setText(student.address);
        CheckBox checkStatus = findViewById(R.id.student_edit_check_status);
        checkStatus.setChecked(student.checkStatus);

        String originalId = student.id;

        Button cancelButton = findViewById(R.id.student_edit_cancel);
        cancelButton.setOnClickListener(view -> {
            setResult(Activity.RESULT_OK, new Intent().putExtra("actionTaken", ActionAtEdit.CANCEL));
            finish();
        });

        Button deleteButton = findViewById(R.id.student_edit_delete);
        deleteButton.setOnClickListener(view -> {
            Model.instance().deleteStudent(id.getText().toString());
            setResult(Activity.RESULT_OK, new Intent().putExtra("actionTaken", ActionAtEdit.DELETE));
            finish();
            startActivity(new Intent(EditStudentActivity.this, MainActivity.class));
        });

        Button saveButton = findViewById(R.id.student_edit_save);
        saveButton.setOnClickListener(view -> {
            Model.instance().updateStudent(new Student(name.getText().toString(),
                                                        id.getText().toString(),
                                                        phone.getText().toString(),
                                                        address.getText().toString(),
                                                        checkStatus.isChecked()), originalId);
            setResult(Activity.RESULT_OK, new Intent().putExtra("actionTaken", ActionAtEdit.SAVE));
            finish();
            startActivity(new Intent(EditStudentActivity.this, MainActivity.class));
        });
    }
}

