package com.example.students_app.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Model {
    private static final Model _instance = new Model();

    public static Model instance(){
        return _instance;
    }
    private Model(){
        for(int i = 1; i <= 20; i++){
            addStudent(new Student("name " + i,
                                "" + i,
                                "" + i,
                                "street number " + i,
                                false));
        }
    }

    List<Student> data = new LinkedList<>();
    public List<Student> getAllStudents(){
        return data;
    }

    public void addStudent(Student student){
        data.add(student);
    }

    public void updateStudent(Student student, String originalId) {
        data.forEach(currentStudent -> {
            if (Objects.equals(currentStudent.id, originalId)) {
                currentStudent.id = student.id;
                currentStudent.name = student.name;
                currentStudent.phone = student.phone;
                currentStudent.address = student.address;
                currentStudent.checkStatus = student.checkStatus;
            }
        });
    }

    public void deleteStudent(String id) {
        for (int i = 0; i < data.size(); i++) {
            if (Objects.equals(data.get(i).id, id)) {
                data.remove(i);
            }
        }
    }
}
