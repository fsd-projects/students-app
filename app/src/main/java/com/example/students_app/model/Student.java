package com.example.students_app.model;

import java.io.Serializable;

public class Student implements Serializable {
    public String name;
    public String id;
    public String phone;
    public String address;
    public String photoUrl;
    public Boolean checkStatus;

    public Student(String name, String id, String phone, String address, Boolean checkStatus) {
        this.name = name;
        this.id = id;
        this.phone = phone;
        this.address = address;
        this.photoUrl = "C:\\Program Files\\Android\\Android Studio\\plugins\\android\\resources\\sampleData\\avatars\\avatar_10.xml";
        this.checkStatus = checkStatus;
    }
}
